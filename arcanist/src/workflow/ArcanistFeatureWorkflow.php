<?php

/**
 * Displays user's Git branches or Mercurial bookmarks.
 *
 * @concrete-extensible
 */
class ArcanistFeatureWorkflow extends ArcanistWorkflow {

  private $branches;

  public function getWorkflowName() {
    return 'feature';
  }

  public function getCommandSynopses() {
    return phutil_console_format(<<<EOTEXT
      **feature** [__options__]
      **feature** __name__ [__start__]
      **feature** start __name__
      **feature** finish __name__
      **feature** delete __name__
EOTEXT
      );
  }

  public function getCommandHelp() {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git branch' or 'hg bookmark'.

          Without __name__, it lists the available branches and their revision
          status.

          With __name__, it creates or checks out a branch. If the branch
          __name__ doesn't exist and is in format D123 then the branch of
          revision D123 is checked out. Use __start__ to specify where the new
          branch will start. Use 'dha.feature.start.default' to set the default
          feature start location.

          With start [__branch name__], This action creates a new feature branch
          based on 'develop' and switches to it.

          With finish, finish the development of a feature. This action performs
          the following :
           - Merges MYFEATURE into 'develop'.
           - Removes the feature branch.
           - Switches back to 'develop' branch.
EOTEXT
      );
  }

  public function requiresConduit() {
    return true;
  }

  public function requiresRepositoryAPI() {
    return true;
  }

  public function requiresAuthentication() {
    return true;
  }

  public function getArguments() {
    return array(
      'view-all' => array(
        'help' => pht('Include closed and abandoned revisions.'),
      ),
      'list' => array(
        'help' => pht('List feature branch'),
      ),
      'by-status' => array(
        'help' => pht('Sort branches by status instead of time.'),
      ),
      'output' => array(
        'param' => 'format',
        'support' => array(
          'json',
        ),
        'help' => pht(
          "With '%s', show features in machine-readable JSON format.",
          'json'),
      ),
      'f' => array(
        'help' => pht('Force deletion'),
      ),
      '*' => 'branch',
    );
  }

  public function getSupportedRevisionControlSystems() {
    return array('git', 'hg');
  }

  public function run() {
    $repository_api = $this->getRepositoryAPI();

    $filter = $this->getArgument('list');

    if ($filter) {
      return $this->filterBranch($repository_api);
    }

    $names = $this->getArgument('branch');
    if ($names) {
      if (count($names) > 2) {
        throw new ArcanistUsageException(pht('Specify only one branch.'));
      }
      if($names[0] == 'start') {
        return $this->startBranch($names, $repository_api);
      }
      if($names[0] == 'finish') {
        return $this->finishBranch($names, $repository_api);
      }
      if($names[0] == 'delete') {
        return $this->deleteBranch($names, $repository_api);
      }
      return $this->checkoutBranch($names);
    }

    $branches = $repository_api->getAllBranches();
    if (!$branches) {
      throw new ArcanistUsageException(
        pht('No branches in this working copy.'));
    }

    $branches = $this->loadCommitInfo($branches);
    $revisions = $this->loadRevisions($branches);
    $this->printBranches($branches, $revisions);

    return 0;
  }

  private function checkoutBranch(array $names) {
    $api = $this->getRepositoryAPI();

    if ($api instanceof ArcanistMercurialAPI) {
      $command = 'update %s';
    } else {
      $command = 'checkout %s';
    }

    $err = 1;

    $name = $names[0];
    if (isset($names[1])) {
      $start = $names[1];
    } else {
      $start = $this->getConfigFromAnySource('dha.feature.start.default');
    }

    $branches = $api->getAllBranches();
    if (in_array($name, ipull($branches, 'name'))) {
      list($err, $stdout, $stderr) = $api->execManualLocal($command, $name);
    }

    if ($err) {
      $match = null;
      if (preg_match('/^D(\d+)$/', $name, $match)) {
        try {
          $diff = $this->getConduit()->callMethodSynchronous(
            'differential.querydiffs',
            array(
              'revisionIDs' => array($match[1]),
            ));
          $diff = head($diff);

          if ($diff['branch'] != '') {
            $name = $diff['branch'];
            list($err, $stdout, $stderr) = $api->execManualLocal(
              $command,
              $name);
          }
        } catch (ConduitClientException $ex) {}
      }
    }

    if ($err) {
      if ($api instanceof ArcanistMercurialAPI) {
        $rev = '';
        if ($start) {
          $rev = csprintf('-r %s', $start);
        }

        $exec = $api->execManualLocal('bookmark %C %s', $rev, $name);

        if (!$exec[0] && $start) {
          $api->execxLocal('update %s', $name);
        }
      } else {
        $startarg = $start ? csprintf('%s', $start) : '';
        $exec = $api->execManualLocal(
          'checkout --track -b %s %C',
          $name,
          $startarg);
      }

      list($err, $stdout, $stderr) = $exec;
    }

    echo $stdout;
    fprintf(STDERR, '%s', $stderr);
    return $err;
  }

  private function startBranch(array $names, $repository_api)
  {
    $task_name = $this->getTaskName($names[1]);
    $develop = false;
    $branches = $repository_api->getAllBranches();
    foreach ($branches as $branch) {
      if($branch['name'] == 'develop' || $branch['name'] == 'development') {
        $develop = $branch['name'];
        break;
      }
    }
    if(count($names) === 1) {
      throw new ArcanistUsageException(pht('Wrong syntax.'));
    }
    if($develop) {
      $exec = $repository_api->execManualLocal(
        'checkout --track -b %s %C',
        'feature/' . $names[1] . $task_name,
        $develop);

      list($err, $stdout, $stderr) = $exec;
      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      // Start traking time a task
      if ($task_name && $err === 0) {
        $out = shell_exec("dha start " . $names[1]);
        echo $out;
      }
      return $err;
    } else {
      throw new ArcanistUsageException(pht('Do not find branch develop or development.'));
    }
  }

  private function finishBranch(array $names, $repository_api) {
    if(count($names) === 1) {
      throw new ArcanistUsageException(pht('Wrong syntax.'));
    }
    exec('git rev-parse --abbrev-ref HEAD', $output);
    $branches = $repository_api->getAllBranches();
    $develop = false;
    $task_name = $this->getTaskName($names[1]);

    exec('git status', $statuses);
    if(in_array('nothing to commit, working tree clean',$statuses)) {
      $treeClean = true;
    } else {
      $treeClean = false;
    }

    foreach ($branches as $branch) {
      if($branch['name'] == 'develop' || $branch['name'] == 'development') {
        $develop = $branch['name'];
        break;
      }
    }
    if($develop) {
      $featureBranch = $names[1];
      $branch = $featureBranch . $task_name;
      if ($task_name) {
        if ($treeClean) {
          $exec = $repository_api->execManualLocal(
            'push origin feature/%s',
            $branch);
          list($err, $stdout, $stderr) = $exec;
          // Stop traking time a task
          if ($err === 0) {
            $out = shell_exec("dha stop " . $featureBranch);
            echo $out;
          }
        } else {
          $exec = $repository_api->execManualLocal(
            'status');
          list($err, $stdout, $stderr) = $exec;
        }
      } else {
        $exec = $repository_api->execManualLocal(
          $treeClean ? 'checkout %s' : 'stash -u && git checkout %s',
          $develop);
        list($err, $stdout, $stderr) = $exec;
        if($err === 0) {
          $exec = $repository_api->execManualLocal(
            'merge feature/%s --no-commit --no-edit', $featureBranch);
          list($err, $stdout, $stderr) = $exec;
          echo $stdout;
          fprintf(STDERR, '%s', $stderr);
        }
        if($treeClean === false && $err === 0) {
          $exec = $repository_api->execManualLocal(
            'stash pop');
        }
        if($err === 0) {
          $exec = $repository_api->execManualLocal(
            'branch -d feature/%s', $featureBranch);
          list($err, $stdout, $stderr) = $exec;
        }
      }
      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      return $err;
    } else {
      throw new ArcanistUsageException(pht('Do not find branch develop or development.'));
    }
  }

  private function deleteBranch(array $names, $repository_api) {
    if(count($names) === 1) {
      throw new ArcanistUsageException(pht('Wrong syntax.'));
    }

    $forceDelete = $this->getArgument('f');
    $branchToDelete = 'feature/' . $names[1];
    list($err) = $repository_api->execManualLocal(
      'rev-parse --verify %s',
      $branchToDelete);

    if ($err) {
      throw new ArcanistUsageException(
        pht("Branch '%s' does not exist.", $branchToDelete));
    }

    exec('git status', $status);
    if (in_array('nothing to commit, working tree clean', $status)) {
      $exec = $repository_api->execManualLocal(
        'checkout %s', 'develop');
      list($err, $stdout, $stderr) = $exec;
      fprintf(STDERR, '%s', $stderr);

      if ($forceDelete) {
        $exec = $repository_api->execManualLocal(
          'branch -D %s', $branchToDelete);
        list($err, $stdout, $stderr) = $exec;
      } else {
        $exec = $repository_api->execManualLocal(
          'branch -d %s', $branchToDelete);
        list($err, $stdout, $stderr) = $exec;

        if ($err)
          $stderr = phutil_console_format(
            "Fatal: Feature branch '{$branchToDelete}' has been not been merged in branch 'master' and/or branch 'develop'. Use --f to force the deletion.\n");
      }
      echo $stdout;
      fprintf(STDERR, '%s', $stderr);
      return $err;
    } else
      echo phutil_console_format(
        "Fatal: Working tree contains unstaged changes. Aborting.\n");
  }


  private function loadCommitInfo(array $branches) {
    $repository_api = $this->getRepositoryAPI();

    $branches = ipull($branches, null, 'name');

    if ($repository_api instanceof ArcanistMercurialAPI) {
      $futures = array();
      foreach ($branches as $branch) {
        $futures[$branch['name']] = $repository_api->execFutureLocal(
          'log -l 1 --template %s -r %s',
          "{node}\1{date|hgdate}\1{p1node}\1{desc|firstline}\1{desc}",
          hgsprintf('%s', $branch['name']));
      }

      $futures = id(new FutureIterator($futures))
        ->limit(16);
      foreach ($futures as $name => $future) {
        list($info) = $future->resolvex();

        $fields = explode("\1", trim($info), 5);
        list($hash, $epoch, $tree, $desc, $text) = $fields;

        $branches[$name] += array(
          'hash' => $hash,
          'desc' => $desc,
          'tree' => $tree,
          'epoch' => (int)$epoch,
          'text' => $text,
        );
      }
    }

    foreach ($branches as $name => $branch) {
      $text = $branch['text'];

      try {
        $message = ArcanistDifferentialCommitMessage::newFromRawCorpus($text);
        $id = $message->getRevisionID();

        $branch['revisionID'] = $id;
      } catch (ArcanistUsageException $ex) {
        // In case of invalid commit message which fails the parsing,
        // do nothing.
        $branch['revisionID'] = null;
      }

      $branches[$name] = $branch;
    }

    return $branches;
  }

  private function loadRevisions(array $branches) {
    $ids = array();
    $hashes = array();

    foreach ($branches as $branch) {
      if ($branch['revisionID']) {
        $ids[] = $branch['revisionID'];
      }
      $hashes[] = array('gtcm', $branch['hash']);
      $hashes[] = array('gttr', $branch['tree']);
    }

    $calls = array();

    if ($ids) {
      $calls[] = $this->getConduit()->callMethod(
        'differential.query',
        array(
          'ids' => $ids,
        ));
    }

    if ($hashes) {
      $calls[] = $this->getConduit()->callMethod(
        'differential.query',
        array(
          'commitHashes' => $hashes,
        ));
    }

    $results = array();
    foreach (new FutureIterator($calls) as $call) {
      $results[] = $call->resolve();
    }

    return array_mergev($results);
  }

  private function printBranches(array $branches, array $revisions) {
    $revisions = ipull($revisions, null, 'id');

    static $color_map = array(
      'Closed'          => 'cyan',
      'Needs Review'    => 'magenta',
      'Needs Revision'  => 'red',
      'Accepted'        => 'green',
      'No Revision'     => 'blue',
      'Abandoned'       => 'default',
    );

    static $ssort_map = array(
      'Closed'          => 1,
      'No Revision'     => 2,
      'Needs Review'    => 3,
      'Needs Revision'  => 4,
      'Accepted'        => 5,
    );

    $out = array();
    foreach ($branches as $branch) {
      $revision = idx($revisions, idx($branch, 'revisionID'));

      // If we haven't identified a revision by ID, try to identify it by hash.
      if (!$revision) {
        foreach ($revisions as $rev) {
          $hashes = idx($rev, 'hashes', array());
          foreach ($hashes as $hash) {
            if (($hash[0] == 'gtcm' && $hash[1] == $branch['hash']) ||
                ($hash[0] == 'gttr' && $hash[1] == $branch['tree'])) {
              $revision = $rev;
              break;
            }
          }
        }
      }

      if ($revision) {
        $desc = 'D'.$revision['id'].': '.$revision['title'];
        $status = $revision['statusName'];
      } else {
        $desc = $branch['desc'];
        $status = pht('No Revision');
      }

      if (!$this->getArgument('view-all') && !$branch['current']) {
        if ($status == 'Closed' || $status == 'Abandoned') {
          continue;
        }
      }

      $epoch = $branch['epoch'];

      $color = idx($color_map, $status, 'default');
      $ssort = sprintf('%d%012d', idx($ssort_map, $status, 0), $epoch);

      $out[] = array(
        'name'      => $branch['name'],
        'current'   => $branch['current'],
        'status'    => $status,
        'desc'      => $desc,
        'revision'  => $revision ? $revision['id'] : null,
        'color'     => $color,
        'esort'     => $epoch,
        'epoch'     => $epoch,
        'ssort'     => $ssort,
      );
    }

    if (!$out) {
      // All of the revisions are closed or abandoned.
      return;
    }

    $len_name = max(array_map('strlen', ipull($out, 'name'))) + 2;
    $len_status = max(array_map('strlen', ipull($out, 'status'))) + 2;

    if ($this->getArgument('by-status')) {
      $out = isort($out, 'ssort');
    } else {
      $out = isort($out, 'esort');
    }
    if ($this->getArgument('output') == 'json') {
      foreach ($out as &$feature) {
        unset($feature['color'], $feature['ssort'], $feature['esort']);
      }
      echo json_encode(ipull($out, null, 'name'))."\n";
    } else {
      $table = id(new PhutilConsoleTable())
        ->setShowHeader(false)
        ->addColumn('current', array('title' => ''))
        ->addColumn('name',    array('title' => pht('Name')))
        ->addColumn('status',  array('title' => pht('Status')))
        ->addColumn('descr',   array('title' => pht('Description')));

      foreach ($out as $line) {
        $table->addRow(array(
          'current' => $line['current'] ? '*' : '',
          'name'    => tsprintf('**%s**', $line['name']),
          'status'  => tsprintf(
            "<fg:{$line['color']}>%s</fg>", $line['status']),
          'descr'   => $line['desc'],
        ));
      }

      $table->draw();
    }
  }

  private function filterBranch($repository_api) {
    $branches = $repository_api->getAllBranches();
    $featureBranches = array();
    foreach ($branches as $branch) {
      if(strpos($branch['name'],"feature") === 0) {
        $featureBranches[] = $branch;
      }
    }
    if (count($featureBranches) === 0) {
      throw new ArcanistUsageException(
        pht('No branches in this working copy.'));
    }

    $featureBranches = $this->loadCommitInfo($featureBranches);
    $revisions = $this->loadRevisions($featureBranches);
    $this->printBranches($featureBranches, $revisions);
  }

  // get task_name by task_id
  private function getTaskName($taskId) {
    $conduit = $this->getConduit();
    $task_name = null;

    if (preg_match('/^T?\d+$/', $taskId)) {
      $task_id = substr($taskId, 1);
      $tasks = $conduit->callMethodSynchronous(
        'maniphest.query',
        array(
          'ids' => array($task_id),
        ));

      if ($tasks) {
        foreach ($tasks as $task) {
          $task_name = '_' . str_replace(' ', '_', $task['title']);
        }
      }
    }

    return $task_name;
  }

}
