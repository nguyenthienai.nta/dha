<?php

/**
 * Stash the changes in a dirty working directory away.
 *
 * @concrete-extensible
 */
class ArcanistStashWorkflow extends ArcanistWorkflow
{

  private $options;

  public function getWorkflowName()
  {
    return 'stash';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **stash** [__options__]
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git stash'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return false;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array(
        '*' => 'option'
    );
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $repository_api = $this->getRepositoryAPI();
    $names = $this->getArgument('option');

    if (count($names) > 2)
        throw new ArcanistUsageException(pht('Wrong syntax.'));

    if (!$names) {
        $exec = $repository_api->execManualLocal('stash');
        list($err, $stdout, $stderr) = $exec;
    } else {
        $option = $names[0];
        $arugement = isset($names[1]) ? $names[1] : null;
        $exec = $repository_api->execManualLocal('stash %s %C', $option, $arugement);
        list($err, $stdout, $stderr) = $exec;
    }

    fprintf(STDERR, '%s', $stderr);
    echo $stdout;
    return $err;
  }

}

