<?php

/**
 * Displays user's Git branches or Mercurial bookmarks.
 *
 * @concrete-extensible
 */
class ArcanistStatusWorkflow extends ArcanistWorkflow
{

  private $status;

  public function getWorkflowName()
  {
    return 'status';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **status**
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git status'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return
      ;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array(
    );
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $repository_api = $this->getRepositoryAPI();
    $exec = $repository_api->execManualLocal(
      'status');
    list($err, $stdout, $stderr) = $exec;
    echo $stdout;
    fprintf(STDERR, '%s', $stderr);
    return $err;
  }

}

