<?php

/**
 * Redirects to `dha backout` workflow.
 */
final class ArcanistRevertWorkflow extends ArcanistWorkflow {

  public function getWorkflowName() {
    return 'revert';
  }

  public function getCommandSynopses() {
    return phutil_console_format(<<<EOTEXT
      **revert**
EOTEXT
    );
  }

  public function getCommandHelp() {
    return phutil_console_format(<<<EOTEXT
    Please use dha backout instead
EOTEXT
    );
  }

  public function getArguments() {
    return array(
      '*' => 'input',
    );
  }

  public function getSupportedRevisionControlSystems() {
    return array('git', 'hg');
  }

  public function run() {
    echo pht(
      'Please use `%s` instead.',
      'dha backout')."\n";
    return 1;
  }

}
