<?php

/**
 * Fetch from and integrate with another repository or a local branch.
 *
 * @concrete-extensible
 */
class ArcanistPullWorkflow extends ArcanistWorkflow
{

  private $branch;

  public function getWorkflowName()
  {
    return 'pull';
  }

  public function getCommandSynopses()
  {
    return phutil_console_format(<<<EOTEXT
      **pull** __branch_name__
EOTEXT
    );
  }

  public function getCommandHelp()
  {
    return phutil_console_format(<<<EOTEXT
          Supports: git, hg
          A wrapper on 'git pull'.
EOTEXT
    );
  }

  public function requiresConduit()
  {
    return false;
  }

  public function requiresRepositoryAPI()
  {
    return true;
  }

  public function getArguments()
  {
    return array(
        '*' => 'branch'
    );
  }

  public function getSupportedRevisionControlSystems()
  {
    return array('git', 'hg');
  }

  public function run()
  {
    $repository_api = $this->getRepositoryAPI();
    $names = $this->getArgument('branch');

    if (count($names) > 2)
        throw new ArcanistUsageException(pht('Wrong syntax.'));

    if (!$names) {
        $exec = $repository_api->execManualLocal('pull');
        list($err, $stdout, $stderr) = $exec;
    } else {
        $remote = $names[0];
        $branchName = isset($names[1]) ? $names[1] : null;
        $exec = $repository_api->execManualLocal('pull %s %C', $remote, $branchName);
        list($err, $stdout, $stderr) = $exec;
    }

    fprintf(STDERR, '%s', $stderr);
    echo $stdout;
    return $err;
  }

}

