
Usage

You can add this library as a local, per-project dependency to your project using Composer:

    composer global require deha/dha


Configure Environment Variables
    1. Edit ~/.bash_profile file:
           $ nano ~/.bash_profile
    2. Add the following lines:
           export PATH="$PATH:~/.config/composer/vendor/bin/"
    3. Type the following command:
           $  source ~/.bash_profile       //reload file .bash_profile

